import sys
import os
import requests
import glob
import getopt

# necessary to bypass cloudflare (TM) (R) protection
headers = {
"Host": "prnt.sc",
"User-Agent": "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0",
"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
"Accept-Language": "de,en-US;q=0.7,en;q=0.3",
#"Accept-Encoding": "gzip, deflate, br",
"Cookie": "__cfduid=dc2405098cf4f7911c2e958e23a3e91ab1535400045; _ga=GA1.2.186162277.1535400047; _gid=GA1.2.401824508.1539975877; _gat=1",
"Connection": "keep-alive",
"Upgrade-Insecure-Requests": "1",
"DNT": "1",
"Pragma": "no-cache",
"Cache-Control": "no-cache"
}

# default values
host = "https://prnt.sc/"
downloadDir = "downloads"
startID = 'blobby'
count = None

def generateIds(start, count = None):
	chars = "0123456789abcdefghijklmnopqrstuvwxyz"
	currentId = list(start)
	while count == None or count > 0:
		yield ''.join(currentId)
		carry = True
		for digit in reversed(range(len(currentId))):
			charIdx = chars.find(currentId[digit]) + 1
			if charIdx >= len(chars):
				currentId[digit] = chars[0]
			else:
				currentId[digit] = chars[charIdx]
				break
		if count != None: count -= 1

def getHtml(url):
	r = requests.get(url, headers=headers)
	return r.text

def getImageUrl(imgId):
	url = host + imgId
	r = requests.get(url, headers=headers)
	html = r.text
	startIdx = html.find("https://image.prntscr.com/image/")
	if startIdx == -1: return ""
	endIdx = html.find("\"", startIdx)
	imageUrl = html[startIdx:endIdx]
	return imageUrl

def saveImg(url, name, ext):
	r = requests.get(url)
	if not os.path.exists(downloadDir):
		os.makedirs(downloadDir)
	filepath = os.path.join(downloadDir, name + '.' + ext)
	open(filepath, "wb").write(r.content)

def isDownloaded(imgId):
	if len(glob.glob(os.path.join(downloadDir, imgId) + ".*"))>0: return True
	return False

def fileType(url):
	if url.endswith(".jpg") or url.endswith(".jpeg"): return "jpg"
	return "png"

def help():
	print('Usage: python3 lightshot-crawler.py [OPTION]')
	print('  -s [ID],     --start=[ID]    start from specific ID. Default: ' + startID)
	print('  -c [NUMBER], --count=[NUMBER]  number of images to download')
	print('  -d [PATH],   --dest=[PATH]     download folder path')
	print('  -h,          --help            print this help')

options, _ = getopt.getopt(sys.argv[1:], "hs:c:d:", ['help', 'start=', 'count=', 'dest='])
for opt, arg in options:
	if opt in ['-h', '--help']:
		help()
		exit()
	if opt in ['-s', '--start']: startID = arg
	if opt in ['-c', '--count']: count = int(arg)
	if opt in ['-d', '--dest']: downloadDir = arg


for imgId in generateIds(startID, count):
	if count != None:
		if count <= 0: break
		count -= 1
	print()
	print("[" + imgId + "]", end="", flush=True)
	if isDownloaded(imgId):
		print(" already downloaded", end="")
		continue
	imgUrl = getImageUrl(imgId)
	if imgUrl == "":
		print(" not available", end="")
		continue
	print(" downloading " + imgUrl, end="", flush=True)
	fType = fileType(imgUrl)
	saveImg(imgUrl, imgId, fType)
	print(" OK", end="")

print('\nexit')
